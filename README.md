
# Glow Effect

This module allows you to easily add an interactive glow effect to HTML elements on mouse movement and hover.

## Installation

Go to /src/GlowEffect
- Copy the folder
- Paste it in your project

    
## Usage/Examples
**1. Apply the glow effect CSS class:**
  
To enable the glow effect on specific HTML elements, follow these steps:  
  
Example:
```HTML
<div class="card glowable">
	<div class="card-content"></div>
</div>
```
Add the CSS class glowable to the HTML element(s) where you want to see the glow effect.  

**2. Import the CSS:**  

In your main CSS file, import the GlowEffect.css file  
Example:  
```CSS
@import url('./src/GlowEffect.css');
```

**3. Import and Initialize the Module:**

In your main JavaScript file, import the module and call the initialization function to activate the glow effect.  

Example:
```javascript
import initGlowEffect from './src/GlowEffect.js'

initGlowEffect()
```

