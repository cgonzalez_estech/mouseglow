export default function initGlowEffect () {
    document.body.addEventListener('mousemove', e => {
        const glowableElt = e.target.closest('.glowable')
        if (glowableElt) {
            if (!glowableElt.classList.contains('transition')) glowableElt.classList.add('transition')
            document.querySelectorAll('.glowable')?.forEach(el => {
                const rect = el.getBoundingClientRect()
                const x = e.clientX - rect.left
                const y = e.clientY - rect.top
                el.style.setProperty('--x', `${x}px`)
                el.style.setProperty('--y', `${y}px`)
            })
            glowableElt.style.setProperty('--glow-color', `#555`)
        } else {
            document.querySelectorAll('.glowable')?.forEach(el => {
                const rect = el.getBoundingClientRect()
                const x = e.clientX - rect.left
                const y = e.clientY - rect.top
                el.style.setProperty('--x', `${x}px`)
                el.style.setProperty('--y', `${y}px`)
            })
        }
    })
      
    document.querySelectorAll('.glowable')?.forEach(el => {
        el.addEventListener('mouseleave', e => {
            e.target.style.setProperty('--glow-color', 'black')
        })
    })
}